/*----- Q1 - the Images -----/
// Q1 - declare THREE VARIABLES and set them respectively with the  filenames of the THREE courses  (check the pics folder for the names of the files)

// --------------------------------------------------------
/*----- Q2 - the Descrs -----/
// declare THREE VARIABLES and set them respectively with the texts given below 

// "JavaScript, often abbreviated as JS, is a programming language that is one of the core technologies of the World Wide Web, alongside HTML and CSS. <br> <br>As of 2023, 98.7% of websites use JavaScript on the client side for webpage behavior, often incorporating third-party libraries",

// "Java is a high-level, class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible." ,

// "Oracle Database is a proprietary multi-model database management system produced and marketed by Oracle Corporation. <br><br>It is a database commonly used for running online transaction processing, data warehousing and mixed database workloads."

// --------------------------------------------------------
/*----- Q3 - the Profs -----/
// create THREE VARIABLES  and set them respectively with the names of the professors: "sam & nasr", "dan & swetha" and "giancarlo & mahsa"

// --------------------------------------------------------
/*----- Q4 - processing -----/
Whenever a button (course chosen) is clicked, update the image, the profs and the descriptive text of the selected course.

Initially, the JS course is set (image, text and professors names).*/

//Q1
const javaScript = "js.png";
const oracle = "oracle.jpg";
const javaPic = "Java-Logo.jpg";
//Q2
const jsText = "JavaScript, often abbreviated as JS, is a programming language that is one of the core technologies of the World Wide Web, alongside HTML and CSS. <br> <br>As of 2023, 98.7% of websites use JavaScript on the client side for webpage behavior, often incorporating third-party libraries";
const javaText = "Java is a high-level, class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible.";
const oracleText = "Oracle Database is a proprietary multi-model database management system produced and marketed by Oracle Corporation. <br><br>It is a database commonly used for running online transaction processing, data warehousing and mixed database workloads.";
//Q3
const jsProf = "sam & nasr";
const jProf = "dan & swetha";
const oracleProf = "giancarlo & masha";

const pic = document.getElementsByTagName("img")[0];
const heading = document.getElementById("heading");
const text = document.getElementById("text");
const btnjs = document.getElementById("js");
const btnj = document.getElementById("java");
const btnoracle= document.getElementById("oracle");

pic.src = javaScript;
pic.alt = "This is a picture of JavaScript";
heading.textContent = jsProf;
text.textContent = jsText;

function jsClicked(event){
    pic.src = javaScript;
    pic.alt = "This is a picture of JavaScript";
    heading.textContent = jsProf;
    text.textContent = jsText;
}

function jClicked(event){
    pic.src = javaPic;
    pic.alt = "This is a picture of Java";
    heading.textContent = jProf;
    text.textContent = javaText;
}

function oracleClicked(event){
    pic.src = oracle;
    pic.alt = "This is a picture of Oracle";
    heading.textContent = oracleProf;
    text.textContent = oracleText;
}

btnjs.addEventListener("click", jsClicked);
btnj.addEventListener("click", jClicked);
btnoracle.addEventListener("click", oracleClicked);

